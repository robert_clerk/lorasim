#pragma once

#include <random>
#include <iostream>
#include "rf_transmission.h"
#include "lora_reciever_channel.h"

class lora_gateway
{
public:

	unsigned int gatewayID;
	float x_pos;
	float y_pos;
	float tx_period = 10.0;
	float message_length = 0.2;
	unsigned int tx_channel;
	float tx_power = 20.0;
	float antenna_gain = 2.0; // dBi Gain
	float EIRP;
	int channel_rx_count[8];
	int channel_rx_successful_count[8];
	int channel_rx_percent_ok[8];
	bool channel_rx_collision[8];

	lora_reciever_channel rx[8];
	
	lora_gateway();
	~lora_gateway();

	void check_status(std::vector<rf_transmission> &rf_transmissions, float sim_time, int tx_new_count);
};

