#pragma once

#include <vector>
#include "rf_transmission.h"

class lora_reciever_channel
{
	
public:

	int rx_total;
	int rx_total_success;
	int rx_failed;
	float packet_loss;
	float packet_success;
	
	std::vector<int> rx_messages_successful;

	bool rx_processing;
	int curent_message_index;
	rf_transmission curent_rf_transmission;
	rf_transmission new_rf_transmission;
	float current_message_stop_time;

	lora_reciever_channel();
	~lora_reciever_channel();
	void check_proccessing_status(std::vector<rf_transmission> &rf_transmissions, float sim_time);
	void process_new_transmission(std::vector<rf_transmission> &rf_transmissions, int rf_transmission_index, float sim_time);
	int rx_total_count();
	int rx_total_success_count();
	int rx_total_fail_count();

};

