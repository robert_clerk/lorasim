#include "lora_node.h"

lora_node::lora_node()
{
	last_tx_time = -30.0+0.3*(rand() % 100); // 30 sec random avg start
	x_pos = 0.1*(rand() % 1000);
	y_pos = 0.1*(rand() % 1000);
	//std::cout << "Node initialised." << std::endl;
}

lora_node::~lora_node()
{
}


int lora_node::check_status(std::vector<rf_transmission> &rf_transmissions, float sim_time)
{
	int return_value = 0;
	if (tx_on)
		// Device is transmitting
	{
		if ((sim_time - tx_start) > message_length)
		{
			// Device will now stop transmitting as message_length time has expired
			tx_on = false;
			return_value = 2;
		}
	}
	else
	{
		// Device is not transmitting
		if ((sim_time - last_tx_time) > tx_period)
		{
			// Device will now start transmitting tx_period after the start of the last transmission
			
			//update device state
			tx_on = true;
			last_tx_time = sim_time;
			tx_start = sim_time;
			tx_channel = rand() % 8;
			
			// create a transmission
			rf_transmission new_rf_tx;
			new_rf_tx.tx_nodeID = nodeID;
			new_rf_tx.tx_node_type = 2; // 0-Noise, 1-LoraGateway, 2-LoraNode
			new_rf_tx.active_transmission = true;
			new_rf_tx.start_time = sim_time;
			new_rf_tx.duration = message_length;
			new_rf_tx.end_time = sim_time + message_length;
			new_rf_tx.tx_channel = tx_channel;
			new_rf_tx.tx_x;
			new_rf_tx.tx_y;
			new_rf_tx.tx_EIRP;
			rf_transmissions.push_back(new_rf_tx);

			// flag new transmission
			return_value = 1;
		}
	}
	return return_value;
}