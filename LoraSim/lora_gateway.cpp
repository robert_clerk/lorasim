#include "lora_gateway.h"


lora_gateway::lora_gateway()
{
	x_pos = 0.1*(rand() % 1000);
	y_pos = 0.1*(rand() % 1000);
	//std::cout << "Gateway initialised." << std::endl;
}

lora_gateway::~lora_gateway()
{

}


void lora_gateway::check_status(std::vector<rf_transmission> &rf_transmissions, float sim_time, int tx_new_count)
{
	int rf_tx_index = 0;
	int rf_new_tx_channel = 0;

	//check status of each reciever
	for (int i = 0; i < 8; i++)
	{
		rx[i].check_proccessing_status(rf_transmissions, sim_time);
	}

	// For each new transmission in period send to reciver for processing
	for (int i = 0; i < tx_new_count; i++)
	{
		rf_tx_index = rf_transmissions.size() - tx_new_count + i; // determine new rf transmission index
		rf_new_tx_channel = rf_transmissions.at(rf_tx_index).tx_channel; // determine new rf transmission channel

		if ((rf_new_tx_channel >= 0) && (rf_new_tx_channel < 8)) // check to see if channel in range
		{

			rx[rf_new_tx_channel].process_new_transmission(rf_transmissions, rf_tx_index, sim_time); // send packet to reciever for processing
		}
	}

}