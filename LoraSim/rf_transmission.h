#pragma once

#include <iostream>

class rf_transmission
{
public:
	// Attributes
	unsigned int messageID;
	unsigned int tx_node_type; // 0-Noise, 1-LoraGateway, 2-LoraNode
	unsigned int tx_nodeID;
	bool active_transmission;
	float start_time;
	float duration;
	float end_time;
	unsigned int tx_channel;
	float tx_x;
	float tx_y;
	float tx_EIRP;


	// Functions
	rf_transmission();
	rf_transmission(unsigned int tx_node_type_val, unsigned int tx_nodeID_val, bool active_transmission_val, float start_time_val, float duration_val, unsigned int tx_channel_val);
	~rf_transmission();
	void print();
};

