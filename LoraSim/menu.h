#pragma once

#include <string>
#include <iostream>
#include <cmath>

class menu
{
public:
	menu();
	~menu();
	void clear_screen();
	void display_banner();
	void display_message(std::string message_string);
	void display_message_raw(std::string message_string);
	int int_query(std::string query_string, int min_bound, int max_bound);
	int float_query(std::string query_string, float min_bound, float max_bound);
	char yn_query(std::string query_string);
	void graph_int(int size, int *data);
};

