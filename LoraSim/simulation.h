#pragma once

#include "lora_gateway.h"
#include "lora_node.h"
#include "rf_transmission.h"
#include <iostream>
#include <vector>
#include "menu.h"

class simulation
{
public:

	int number_of_nodes = 1;
	int number_of_gateways = 1;
	float sim_duration = 100.0;
	float sim_time = 0.0;
	float sim_time_delta = 0.01; // 10 ms
	
	std::vector<lora_gateway> lora_gateways;
	std::vector<lora_node> lora_nodes;
	std::vector<rf_transmission> rf_transmissions;

	simulation();
	simulation(int nodes, int gateways, float duration, float node_message_length, float node_avg_tx_period, float node_tx_period_variation);
	~simulation();
	void run_sim(menu &m);
	void print_node_sim_tx(int node_id);
	void print_gw_rx_sim_tx(int gw_id, int channel_id);
	void print_all_sim_tx();
	void print_gw_stats(int gw_id);
};

