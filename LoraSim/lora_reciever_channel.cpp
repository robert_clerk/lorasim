#include "lora_reciever_channel.h"


lora_reciever_channel::lora_reciever_channel()
{
	rx_total = 0;
	rx_total_success = 0;
	rx_failed = 0;
	packet_loss = 0.0;
	packet_success = 0.0;
	rx_processing = false;
	curent_message_index = 0;
	current_message_stop_time = 0.0;
}


lora_reciever_channel::~lora_reciever_channel()
{
}


void lora_reciever_channel::check_proccessing_status(std::vector<rf_transmission> &rf_transmissions, float sim_time)
{
	if (rx_processing)
	{

		// check if current recieved transmission is finished processing
		curent_rf_transmission = rf_transmissions.at(curent_message_index);
		if (curent_rf_transmission.end_time < sim_time)
		{
			// message has finished tx/rx, message sucess!
			rx_messages_successful.push_back(curent_message_index); // add to successful rx record
			rx_total_success++;
			packet_success = rx_total_success / rx_total;
			rx_processing = false; // set rx state to non recieveing
			curent_message_index = 0; // set message index to 0
		}
	}
}

void lora_reciever_channel::process_new_transmission(std::vector<rf_transmission> &rf_transmissions, int rf_transmission_index, float sim_time)
{

	rx_total++;
	packet_success = rx_total_success / rx_total;
	new_rf_transmission = rf_transmissions.at(rf_transmission_index);

	if (rx_processing)
	{
		// collision detected			note: add here rx level discrimination
		rx_processing = false;
		curent_message_index = 0; // set message index to 0
		rx_failed++;
	}
	else
	{
		// start processing new message
		rx_processing = true;
		curent_message_index = rf_transmission_index;
	}

}

int lora_reciever_channel::rx_total_count() { return rx_total; }
int lora_reciever_channel::rx_total_success_count() { return rx_total_success; }
int lora_reciever_channel::rx_total_fail_count() { return rx_failed; }