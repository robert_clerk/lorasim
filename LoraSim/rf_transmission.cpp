#include "rf_transmission.h"

rf_transmission::rf_transmission()
{
	// create a unique ID by setting a static variable at increments on each new instance
	static unsigned int messageID_counter = 0; 
	messageID = messageID_counter++;
}

rf_transmission::rf_transmission(unsigned int tx_node_type_val, unsigned int tx_nodeID_val, bool active_transmission_val, float start_time_val, float duration_val, unsigned int tx_channel_val)
	: tx_node_type{ tx_node_type_val }, tx_nodeID{ tx_nodeID_val }, active_transmission{ active_transmission_val }, start_time{ start_time_val }, duration{ duration_val }, tx_channel{ tx_channel_val }
{
	// create a unique ID by setting a static variable at increments on each new instance
	static unsigned int messageID_counter = 0;
	messageID = messageID_counter++;
}

rf_transmission::~rf_transmission()
{
}

void rf_transmission::print()
{
	// Print a rf transmission
	std::cout << start_time << ",TX_ID:" << messageID << ", Type:" << tx_node_type << ", NodeID:" << tx_nodeID << ", Duration:" << duration << ", Channel:" << tx_channel << std::endl;
}
