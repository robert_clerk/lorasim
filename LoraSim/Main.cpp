#include <iostream>
#include <string>
#include <random>

#include "simulation.h"
#include "menu.h"

using namespace std;

int main() {

	// Variable declarations
	int number_of_nodes=1;
	float node_message_length;
	float node_avg_tx_period=160.0;
	float node_tx_period_variation = 30.0;
	int number_of_gateways = 1;
	float sim_duration = 10;
	bool terminated = false;
	menu m;


	while (!terminated)
	{
		// Display banner
		m.clear_screen();
		m.display_banner();

		// Get user inputs
		number_of_nodes = m.int_query("Enter the number of nodes (1-10000)", 1, 10000);
		node_message_length = m.float_query("Enter node message length (10-400 ms)", 10, 400) / 1000.0;
		node_avg_tx_period = m.float_query("Enter node avg. tx cycle (10-600sec)", 10, 600);
		node_tx_period_variation = m.float_query("Enter node tx cycle random variation magnitude (0-120sec)", 0, 120);
		number_of_gateways = m.int_query("Enter the number of gateways (1-20)", 1, 20);
		sim_duration = m.float_query("Enter simulation duration (1-3600sec)", 1, 3600);
		
		// Run the simulation
		simulation sim(number_of_nodes, number_of_gateways, sim_duration, node_message_length, node_avg_tx_period, node_tx_period_variation);
		sim.run_sim(m);

		sim.print_gw_stats(1); // print gw #1 stats

		// Show rf transmissions
		if (m.yn_query("Display node rf transmissions?") == 'y') {
			int node_id = m.int_query("Enter the node id.", 1, number_of_nodes);
			sim.print_node_sim_tx(node_id);
		}

		// Run again?
		if (m.yn_query("Run again?") == 'n') terminated = true;
	}

	return 0;
}