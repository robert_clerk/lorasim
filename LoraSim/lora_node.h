#pragma once

#include <random>
#include <iostream>
#include "rf_transmission.h"

class lora_node
{
public:

	unsigned int nodeID;
	float tx_period = 10.0;
	float message_length = 0.4;
	unsigned int tx_channel;
	float tx_power = 20.0;
	float antenna_gain = 2.0; // dBi Gain
	float EIRP;
	float x_pos;
	float y_pos;
	unsigned int class_type; // 0-undefined, 1-Class A, 2-Class B, 3-Class C
	float last_tx_time = 0.0;
	bool tx_on = false;
	float tx_start = 0.0;


	lora_node();
	~lora_node();
	int check_status(std::vector<rf_transmission> &rf_transmissions, float sim_time);

};

