#include "menu.h"


menu::menu()
{
}

menu::~menu()
{
}

void menu::clear_screen()
{
	system("cls");
}

void menu::display_banner()
{
	std::cout << "========================================================================" << std::endl;
	std::cout << " _            ____       __        __ _     _   _   ____   _            " << std::endl;
	std::cout << "| |     ___  |  _ \\  __ _\\ \\      / // \\   | \\ | | / ___| (_) _ __ ___  " << std::endl;
	std::cout << "| |    / _ \\ | |_) |/ _` |\\ \\ /\\ / // _ \\  |  \\| | \\___ \\ | || '_ ` _ \\ " << std::endl;
	std::cout << "| |___| (_) ||  _ <| (_| | \\ V  V // ___ \\ | |\\  |  ___) || || | | | | |" << std::endl;
	std::cout << "|_____|\\___/ |_| \\_\\\\__,_|  \\_/\\_//_/   \\_\\|_| \\_| |____/ |_||_| |_| |_|" << std::endl;
	std::cout << "                                                                        " << std::endl;
	std::cout << "Release 0_1a" << std::endl;
	std::cout << "Copyright (C) 2011 Multipath Media (all rights reserved)" << std::endl;
	std::cout << "### Unlicenced Copy ###" << std::endl;
	std::cout << "========================================================================" << std::endl << std::endl;
}

void menu::display_message(std::string message_string)
{
	std::cout << "#> " << message_string << std::endl;
}

void menu::display_message_raw(std::string message_string)
{
	std::cout << message_string;
}

int menu::int_query(std::string query_string, int min_bound, int max_bound)
{
	int user_entry=0;
	while (true)
	{
		std::cout << "#> " << query_string;
		std::cout << " >> ";
		if (std::cin >> user_entry)
		{
			if ((user_entry >= min_bound) && (user_entry <= max_bound)) break;
		}
		else
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
	}
	return user_entry;
}

int menu::float_query(std::string query_string, float min_bound, float max_bound)
{
	float user_entry = 0;
	while (true)
	{
		std::cout << "#> " << query_string;
		std::cout << " >> ";
		if (std::cin >> user_entry)
		{
			if ((user_entry >= min_bound) && (user_entry <= max_bound)) break;
		}
		else
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
	}
	return user_entry;
}

char menu::yn_query(std::string query_string)
{
	char user_entry;
	while (true)
	{
		std::cout << "#> " << query_string << " (y/n) >> ";
		if (std::cin >> user_entry)
		{
			if ((user_entry=='y') || (user_entry == 'n')) break;
		}
		else
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
	}
	return user_entry;
}


void menu::graph_int(int size, int *data)
{
	// Example call
	// int n[8] = { 20, 30, 5, 67 , 10, 34, 54, 9 };
	// m.graph_int(8, n);
	

	int element = 0, max = data[0], min = data[0];
	float y_span,y_scale;
	
	// find min / max
	for (int i = 0; i < size; i++)
	{
		element = data[i];
		if (element > max) max = element;
		if (element < min) min = element;
	}

	// calculate veritcal scale
	y_span = max - min;
	y_scale = 10.0 / y_span;


	// plot top border
	for (int i = 0; i < (size + 2); i++) std::cout << "-";
	std::cout << std::endl;

	//plot each row
	for (int j = 0; j < 10; j++)
	{

		std::cout << "|";

		for (int i = 0; i < size; i++)
		{
			element = data[i];

			if (floor(y_scale*element)==(10-j)) std::cout << "x";
			else std::cout << ".";
			
		}
		if (j == 0) std::cout << "| " << max << std::endl;
		else if (j==9) std::cout << "| " << min << std::endl;
		else std::cout << "|" << std::endl;

	}

	// plot bottom border
	for (int i = 0; i < (size + 2); i++) std::cout << "-";
	std::cout << std::endl;

}
