#include "mapping.h"

mapping::mapping()
{
}

mapping::~mapping()
{
}

float mapping::distance(float x_pos1, float y_pos1, float x_pos2, float y_pos2)
{
	return sqrt((x_pos2 - x_pos1)*(x_pos2 - x_pos1) + (y_pos2 - y_pos1)*(y_pos2 - y_pos1));
}