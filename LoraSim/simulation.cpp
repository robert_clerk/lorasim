#include "simulation.h"

#include <iostream>

simulation::simulation()
{
}

simulation::~simulation()
{
}

simulation::simulation(int nodes, int gateways, float duration, float node_message_length, float node_avg_tx_period, float node_tx_period_variation)
{
	std::cout << "#> Initialisation start." << std::endl;
	
	number_of_nodes = nodes;
	number_of_gateways = gateways;
	sim_duration = duration;

	lora_gateways.resize(number_of_gateways);
	lora_nodes.resize(number_of_nodes);
	
	for (int i = 0; i < number_of_nodes; i++)
	{
		lora_nodes[i].nodeID = i;
		lora_nodes[i].message_length = node_message_length;
		lora_nodes[i].tx_period = node_avg_tx_period + node_tx_period_variation*(0.01*(rand() % 100));
	}
	for (int i = 0; i < number_of_gateways; i++)
	{
		lora_gateways[i].gatewayID = i;
	}

	std::cout << "#> Initialisation completed." << std::endl;
}

void simulation::print_all_sim_tx()
{
	for (int i = 0; i<rf_transmissions.size(); i++)
	{
		rf_transmissions.at(i).print();
	}
}

void simulation::print_node_sim_tx(int node_id)
{
	for (int i = 0; i<rf_transmissions.size(); i++)
	{
		if (rf_transmissions.at(i).tx_nodeID==node_id) rf_transmissions.at(i).print();
	}
}

void simulation::print_gw_rx_sim_tx(int gw_id, int channel_id)
{
	for (int i = 0; i<lora_gateways[gw_id].rx[channel_id].rx_messages_successful.size(); i++)
	{
		int rf_transmission_id = lora_gateways[gw_id].rx[channel_id].rx_messages_successful.at(i);
		rf_transmissions.at(rf_transmission_id).print();
	}
}

void simulation::print_gw_stats(int gw_id)
{
	display_message("Gateway #1 Stats");

	const char * format = "%.1f \t%.1f \t%.1f \t%.1f\n";
	printf("channel\ttotal\tdemod\tpercent\n");
	printf("-----\t-----\t-----\t-----\n");
	printf(format, 0, lora_gateways[0].rx[0].rx_total_count()), lora_gateways[0].rx[0].rx_total_success_count(), ceil(2.3), trunc(2.3));
	printf(format, 1, round(3.8), floor(3.8), ceil(3.8), trunc(3.8));


	m.display_message_raw("#> GW:1,CH:0 - RX Count:" + std::to_string(lora_gateways[0].rx[0].rx_total_count()));
	m.display_message_raw(", RX Success Count:" + std::to_string(lora_gateways[0].rx[0].rx_total_success_count()));
	m.display_message_raw("\n");
	float percent = (100.0*lora_gateways[0].rx[0].rx_total_success_count()) / lora_gateways[0].rx[0].rx_total_count();
	std::cout << percent << std::endl;
	m.display_message_raw("#> GW:1,CH:1 - RX Count:" + std::to_string(lora_gateways[0].rx[1].rx_total_count()));
	m.display_message_raw(", RX Success Count:" + std::to_string(lora_gateways[0].rx[1].rx_total_success_count()));
	m.display_message_raw("\n");

}

void simulation::run_sim(menu &m)
{
	// Variable declarations
	int percent_complete = 0;
	int graph_count = 0;

	// Display message and progress bar legend
	m.display_message("Simulation start.");
	m.display_message_raw("|====1====2====3====4====5====6====7====8====9====|\n");
	m.display_message_raw("*");
	
	// Main simualtion loop
	while (sim_time < sim_duration) {

		// Check the status of the nodes
		int tx_new_count = 0;
		for (int i = 0; i < number_of_nodes; i++)
		{
			if (lora_nodes[i].check_status(rf_transmissions, sim_time) == 1) tx_new_count++;
		}

		// Check the status of the gateways
		for (int i = 0; i < number_of_gateways; i++)
		{
			lora_gateways[i].check_status(rf_transmissions, sim_time, tx_new_count);
		}



		// Increment simulation time
		sim_time += sim_time_delta;

		// Update the progress bar
		percent_complete = int(100.0*sim_time / sim_duration);
		if (percent_complete > (2 * (graph_count + 1))) {
			graph_count++;
			m.display_message_raw("*");
		}

	}

	// Display completion message
	m.display_message_raw("*\n");
	m.display_message("Simulation completed.");

	m.display_message_raw("#> Generated " + std::to_string(rf_transmissions.size()));	m.display_message_raw(" rf transmissions.\n");


	

}